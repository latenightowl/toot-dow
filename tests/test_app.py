# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import datetime

from toot_dow.app import DayOfWeekApp

APP = DayOfWeekApp()


def test_get_possibilities__monday():
    timestamp = datetime.datetime(2022, 11, 7, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_monday}


def test_get_possibilities__tuesday():
    timestamp = datetime.datetime(2022, 11, 8, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_tuesday}


def test_get_possibilities__wednesday():
    timestamp = datetime.datetime(2022, 11, 9, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_wednesday}


def test_get_possibilities__thursday():
    timestamp = datetime.datetime(2022, 11, 10, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_thursday}


def test_get_possibilities__friday():
    timestamp = datetime.datetime(2022, 11, 11, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_friday}


def test_get_possibilities__saturday():
    timestamp = datetime.datetime(2022, 11, 12, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_saturday, APP.do_weekend}


def test_get_possibilities__sunday():
    timestamp = datetime.datetime(2022, 11, 13, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert possibilities == {APP.do_weekend}


def test_get_possibilities__every19():
    timestamp = datetime.datetime(2022, 11, 19, 9, 0, 0)
    possibilities = APP._get_possibilities(timestamp)
    assert APP.do_saturday not in possibilities
    assert possibilities == {APP.do_every19}
