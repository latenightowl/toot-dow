# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class SaturdayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["saturday", "ToyStory", "AllTheWayToWork"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "saturday.mp4"
        self.attachment = Attachment(
            image,
            description=(
                "A scene from Toy Story 2. "
                "Al McWhiggin walks out of a building, "
                "sits into a car and crosses large street in front of his workplace, "
                "Al's Toy Barn. Meanwhile, he says: "
                """\"I can't believe I have to drive all the work on a Saturday. """
                """All the way to work!\", while in his chicken costume."""
            ),
        )

    def __str__(self):
        return self.tags
