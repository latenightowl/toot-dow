# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class Every19Message(Message):
    def __init__(self):
        super().__init__(None, tags=["19th", "cat"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "every19.gif"
        self.attachment = Attachment(
            image,
            description=(
                "A gif of a grey kitten on a bed. Description reads: "
                '"post this cat the 19th of every month".'
            ),
            focus=(0, -1),
        )

    def __str__(self):
        return self.tags
