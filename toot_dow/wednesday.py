# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class WednesdayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["wednesday", "WhatAWeekHuh"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "wednesday.jpg"
        self.attachment = Attachment(
            image,
            description=(
                """"What a week, huh?" says Captain Haddock. """
                """"Captain, it's Wednesday" responds Tintin."""
            ),
        )

    def __str__(self):
        return self.tags
