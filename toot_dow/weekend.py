# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class WeekendMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["weekend", "cat"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "weekend.jpg"
        self.attachment = Attachment(
            image,
            description=(
                "An image of a cat, looking very closely into a camera. "
                "A title says: THIS IS A WEEKEND POST, "
                "don't even think about reposting this on a weekday."
            ),
        )

    def __str__(self):
        return self.tags
