# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class MondayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["monday", "loud", "WhenItMonday"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "monday.mp4"
        self.attachment = Attachment(
            image,
            description=(
                """3D render of a pink creature walking on blue and yellow """
                """chessboard. The text in the middle of the screen reads """
                """\"WHEN IT MONDAY\". A really unpleasant, bass-boosted """
                """tune plays loudly in a background."""
            ),
        )

    def __str__(self):
        return self.tags
