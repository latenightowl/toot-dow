# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class Dec01Message(Message):
    def __init__(self):
        super().__init__(None, tags=["december", "christmas"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "1201.mp4"
        self.attachment = Attachment(
            image,
            description=(
                "An animation of Peepo the frog waking up, looking at their "
                "calendar and seeing that it is the December 1st. "
                "He goes outside, where is a snow on a ground and Christmas "
                "decorations all over. "
                '"It\'s the most wonderful time of the year" plays.'
            ),
            focus=(0, -1),
        )

    def __str__(self):
        return self.tags
