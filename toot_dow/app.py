# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
import argparse
import datetime
import random
from typing import Callable, Set

from loguru import logger

from toot_framework import App
from toot_dow.every19 import Every19Message
from toot_dow.monday import MondayMessage
from toot_dow.tuesday import TuesdayMessage
from toot_dow.wednesday import WednesdayMessage
from toot_dow.thursday import ThursdayMessage
from toot_dow.friday import FridayMessage
from toot_dow.weekend import WeekendMessage
from toot_dow.saturday import SaturdayMessage
from toot_dow.dec01 import Dec01Message


class DayOfWeekApp(App):
    def __init__(self):
        super().__init__("toot-dow", "en")

    @classmethod
    def do_nothing(cls) -> None:
        cls()
        logger.info("Doing nothing. Shutting down.")

    @classmethod
    def do_every19(cls) -> None:
        self = cls()
        message = Every19Message()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_dec01(cls) -> None:
        self = cls()
        message = Dec01Message()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_monday(cls) -> None:
        self = cls()
        message = MondayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_tuesday(cls) -> None:
        self = cls()
        message = TuesdayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_wednesday(cls) -> None:
        self = cls()
        message = WednesdayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_thursday(cls) -> None:
        self = cls()
        message = ThursdayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_friday(cls) -> None:
        self = cls()
        message = FridayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_saturday(cls) -> None:
        self = cls()
        message = SaturdayMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def do_weekend(cls) -> None:
        self = cls()
        message = WeekendMessage()
        toot: dict = self.toot(message, attachments=[message.attachment])
        logger.info(f"Toot sent as {toot['url']}.")

    @classmethod
    def _get_possibilities(cls, timestamp: datetime.datetime) -> Set[Callable]:
        """Get a set of possible callables.

        :param timestamp: The date to use.
        :return: List of possibilities.
        """
        # I know there is a better way to do this
        possibilities: Set[Callable] = {*()}

        if timestamp.day == 19:
            possibilities.add(cls.do_every19)
            return possibilities
        if timestamp.day == 1 and timestamp.month == 12:
            possibilities.add(cls.do_dec01)
            return possibilities

        # Monday is 0 and Sunday is 6
        # I have no idea why this is not the same as with cron jobs
        if timestamp.weekday() == 0:
            possibilities.add(cls.do_monday)
        if timestamp.weekday() == 1:
            possibilities.add(cls.do_tuesday)
        if timestamp.weekday() == 2:
            possibilities.add(cls.do_wednesday)
        if timestamp.weekday() == 3:
            possibilities.add(cls.do_thursday)
        if timestamp.weekday() == 4:
            possibilities.add(cls.do_friday)
        if timestamp.weekday() == 5:
            possibilities.add(cls.do_weekend)
            possibilities.add(cls.do_saturday)
        if timestamp.weekday() == 6:
            possibilities.add(cls.do_weekend)

        return possibilities

    @classmethod
    def do_something(cls, timestamp: datetime.datetime) -> None:
        """Post something, based on a given date."""
        possibilities = cls._get_possibilities(timestamp)
        if not possibilities:
            logger.info("Could not pick a message. Shutting down.")
            return
        fn = random.choice(list(possibilities))
        logger.info(f"Picked {fn.__name__}.")
        fn()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--nothing",
        action="store_true",
        help="try to login and exit",
    )
    parser.add_argument(
        "--something",
        action="store_true",
        help="pick a right message at random, toot it and exit",
    )

    options = (
        "monday",
        "tuesday",
        "wednesday",
        "thursday",
        "friday",
        "saturday",
        "weekday",
        "every19",
    )
    for option in options:
        parser.add_argument(
            f"--{option}",
            action="store_true",
            help=f"send {option} toot and exit",
        )

    args = parser.parse_args()
    if args.nothing:
        DayOfWeekApp.do_nothing()
        return
    if args.something:
        DayOfWeekApp.do_something(datetime.datetime.utcnow())
        return

    for option in options:
        if getattr(args, option, False):
            fn = getattr(DayOfWeekApp, f"do_{option}")
            fn()
            return

    parser.print_help()
