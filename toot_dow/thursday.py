# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class ThursdayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["thursday", "WhatAConcept"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "thursday.jpg"
        self.attachment = Attachment(
            image,
            description=(
                "A screenshot from the series Russan Doll. "
                "Woman with long brown wavy hair is holding a cigarette "
                "and is talking to someone in front of her. "
                'Captions read: "Thursday. What a concept.'
            ),
        )

    def __str__(self):
        return self.tags
