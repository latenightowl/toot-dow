# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class FridayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["friday", "TodayIsFridayInCalifornia"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "friday.webm"
        self.attachment = Attachment(
            image,
            description=(
                "A scene from Japanese series Ninja Sentai Kakuranger. "
                "A man dressed in police uniform gets from under a blanket "
                "in a hospital. He says to a surprised man nearby: "
                '"Today is Friday in California", with a strong accent. '
                'The other man just asks: "Huh?" '
                'The officer says "Shoot" and starts firing his gun.'
            ),
        )

    def __str__(self):
        return self.tags
