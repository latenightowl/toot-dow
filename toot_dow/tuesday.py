# SPDX-FileCopyrightText: 2022-present @latenightowl
#
# SPDX-License-Identifier: MIT
from pathlib import Path

from toot_framework import Message, Attachment


class TuesdayMessage(Message):
    def __init__(self):
        super().__init__(None, tags=["tuesday", "cat"])

        parent = Path(__file__).parent
        image: Path = parent / "media" / "tuesday.jpg"
        self.attachment = Attachment(
            image,
            description=(
                "A photograph of orange kitten, with mouth wide open. "
                'A title reads: "AAAAHHHH NOW ITS TUESDAY"'
            ),
        )

    def __str__(self):
        return self.tags
