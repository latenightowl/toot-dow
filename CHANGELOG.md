# CHANGELOG

**1.3.1 (2022-12-22)**

- Wednesday: Add missing quotation mark

**1.3.0 (2022-12-01)**

- December 1st

**1.2.0 (2022-11-14)**

- Tuesday

**1.1.2 (2022-11-11)**

- Friday: Fix file path

**1.1.1 (2022-11-09)**

- Fix `--something`

**1.1.0 (2022-11-08)**

- Friday

**1.0.0 (2022-11-08)**

- The code is reliable enough

**0.0.0 (2022-11-08)**

- Initial release
