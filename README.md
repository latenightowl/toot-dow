# toot-dow

-----

**Table of Contents**

- [About](#about)
- [Usage](#usage)
- [License](#license)

## About

`toot-dow` toots memes related to the current day.
Features will appear as I have time to do them.

Official instance lives as <a rel="me" href="https://botsin.space/@dow">@dow@botsin.space</a>.

## Usage

Install with

```bash
python3 -m pip install git+https://codeberg.org/latenightowl/toot-dow.git
```

or clone the repository and use Hatch, to develop locally.

To use it, you will need an account on some Mastodon server.
The application will cache your environment variables, so you only need to use them the first time.

```bash
TOOT_SERVER=https://mastodon.example.org TOOT_EMAIL=me@example.com TOOT_PASSWORD=password toot-dow --nothing
```

If everything works, you will see some logs and a note that the bot was able to log in.

Next time, you only have to run

```bash
toot-dow --nothing
```

You can use cron to trigger the feature(s).


## License

`toot-dow` is distributed under the terms of the [MIT](https://spdx.org/licenses/MIT.html) license.
